/*  Copyright (C) 2023 Patrick Csikos (https://zelikos.dev)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authored by Patrick Csikos <pcsikos@zelikos.dev>
 */

use adw::subclass::prelude::*;
use gtk::glib::Properties;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::application::TypestApplication;
use crate::config::PROFILE;
use crate::enums::{SessionLength, TextType};
use crate::utils;

mod imp {
    use super::*;

    use std::cell::Cell;

    #[derive(Debug, Properties, gtk::CompositeTemplate)]
    #[template(resource = "/dev/zelikos/typest/ui/window.ui")]
    #[properties(wrapper_type = super::TypestWindow)]
    pub struct TypestWindow {
        #[template_child]
        pub test_text: TemplateChild<gtk::TextView>,

        #[property(get, set = Self::set_text_type, explicit_notify, builder(TextType::default()))]
        pub text_type: Cell<TextType>,

        #[property(get, set = Self::set_session_length, explicit_notify, builder(SessionLength::default()))]
        pub session_length: Cell<SessionLength>,
    }

    impl Default for TypestWindow {
        fn default() -> Self {
            Self {
                test_text: TemplateChild::default(),

                text_type: Cell::default(),

                session_length: Cell::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TypestWindow {
        const NAME: &'static str = "TypestWindow";
        type Type = super::TypestWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();

            // Set up actions
            // klass.install_action("win.roll-dice", None, move |win, _, _| {
            //     win.roll_dice();
            // });

            // klass.install_action("win.show-toast", Some("(si)"), move |win, _, var| {
            //     if let Some((ref toast, i)) = var.and_then(|v| v.get::<(String, i32)>()) {
            //         win.show_toast(toast, adw::ToastPriority::__Unknown(i));
            //     }
            // });
        }
        // You must call `Widget`'s `init_template()` within `instance_init()`
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl TypestWindow {
        fn set_text_type(&self, text_type: TextType) {
            if text_type != self.text_type.replace(text_type) {
                self.obj().notify_text_type();
            }
        }

        fn set_session_length(&self, session_length: SessionLength) {
            if session_length != self.session_length.replace(session_length) {
                self.obj().notify_session_length();
            }
        }

        #[template_callback]
        fn text_type_name(item: adw::EnumListItem) -> String {
            let type_ = TextType::from(item.value() as u32);
            type_.to_translatable_string()
        }

        #[template_callback]
        fn session_length_name(item: adw::EnumListItem) -> String {
            let type_ = SessionLength::from(item.value() as u32);
            type_.to_translatable_string()
        }

        #[template_callback]
        fn on_notify_text_type(
            drop_down: gtk::DropDown,
            _: glib::ParamSpec,
            window: super::TypestWindow,
        ) {
            window.generate_test_text();
        }

        #[template_callback]
        fn on_notify_session_length(
            drop_down: gtk::DropDown,
            _: glib::ParamSpec,
            window: super::TypestWindow,
        ) {
            window.generate_test_text();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TypestWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // obj.setup_actions();
            obj.setup_settings();

            obj.generate_test_text();
        }
    }
    impl WidgetImpl for TypestWindow {}
    impl WindowImpl for TypestWindow {}
    impl ApplicationWindowImpl for TypestWindow {}
    impl AdwApplicationWindowImpl for TypestWindow {}
}

glib::wrapper! {
    pub struct TypestWindow(ObjectSubclass<imp::TypestWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Root;
}

impl TypestWindow {
    pub fn new(app: &TypestApplication) -> Self {
        glib::Object::builder().property("application", app).build()
    }

    // fn setup_actions(&self) {
    //     self.action_set_enabled("win.clear-history", false);
    //     self.action_set_enabled("win.undo-clear", false);
    //     self.action_set_enabled("win.copy-latest", false);
    // }

    fn generate_test_text(&self) {
        let imp = self.imp();

        let session_time: usize = match self.session_length() {
            SessionLength::FifteenSeconds => 15,
            SessionLength::ThirtySeconds => 30,
            SessionLength::OneMinute => 60,
            SessionLength::TenMinutes => 600,
            SessionLength::OneHour => 6000,
        };

        // Have number of words be a multiple of the session length
        // A multiple of 5 is chosen to ensure we exceed the fastest known typing speed
        let num_words = session_time * 5;

        let generated_text = match self.text_type() {
            TextType::Simple => {
                let text = utils::simple_test_text(num_words);
                log::debug!("Simple session of length: {} seconds", session_time);
                text
            }
            TextType::Hard => {
                let text = utils::hard_test_text(num_words);
                log::debug!("Hard session of length: {} seconds", session_time);
                text
            }
            TextType::Custom => {
                let text = utils::custom_test_text();
                log::debug!("Custom session of indefinite length");
                text
            }
        };

        imp.test_text.buffer().set_text(&generated_text);
    }

    fn setup_settings(&self) {
        let settings = utils::settings_manager();
        settings.bind("window-width", self, "default-width").build();
        settings
            .bind("window-height", self, "default-height")
            .build();
        settings.bind("window-maximized", self, "maximized").build();
        settings.bind("text-type", self, "text-type").build();
        settings
            .bind("session-length", self, "session-length")
            .build();
    }

    // fn show_toast(&self, text: impl AsRef<str>, priority: adw::ToastPriority) {
    //     let imp = self.imp();

    //     let toast = adw::Toast::new(text.as_ref());
    //     toast.set_priority(priority);
    //     toast.set_timeout(1);

    //     imp.toast_overlay.add_toast(toast);
    // }
}
