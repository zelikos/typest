/*  Copyright (C) 2023 Patrick Csikos (https://zelikos.dev)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authored by Patrick Csikos <pcsikos@zelikos.dev>
 */

use gettextrs::gettext;
use gtk::glib;

#[derive(Default, Debug, Copy, Clone, glib::Enum, PartialEq)]
#[repr(u32)]
#[enum_type(name = "TextType")]
pub enum TextType {
    #[default]
    Simple,
    Hard,
    Custom,
}

impl TextType {
    pub fn to_translatable_string(self) -> String {
        match self {
            Self::Simple => gettext("Simple"),
            Self::Hard => gettext("Hard"),
            Self::Custom => gettext("Custom"),
        }
    }
}

impl From<u32> for TextType {
    fn from(u: u32) -> Self {
        match u {
            0 => Self::Simple,
            1 => Self::Hard,
            2 => Self::Custom,
            _ => Self::default(),
        }
    }
}

#[derive(Default, Debug, Copy, Clone, glib::Enum, PartialEq)]
#[repr(u32)]
#[enum_type(name = "SessionLength")]
pub enum SessionLength {
    #[default]
    #[enum_value(name = "15", nick = "15 seconds")]
    FifteenSeconds,
    #[enum_value(name = "30", nick = "30 seconds")]
    ThirtySeconds,
    #[enum_value(name = "60", nick = "1 minute")]
    OneMinute,
    #[enum_value(name = "600", nick = "10 minutes")]
    TenMinutes,
    #[enum_value(name = "6000", nick = "1 hour")]
    OneHour,
}

impl SessionLength {
    pub fn to_translatable_string(self) -> String {
        match self {
            Self::FifteenSeconds => gettext("15 seconds"),
            Self::ThirtySeconds => gettext("30 seconds"),
            Self::OneMinute => gettext("1 minute"),
            Self::TenMinutes => gettext("10 minutes"),
            Self::OneHour => gettext("1 hour"),
        }
    }
}

impl From<u32> for SessionLength {
    fn from(u: u32) -> Self {
        match u {
            0 => Self::FifteenSeconds,
            1 => Self::ThirtySeconds,
            2 => Self::OneMinute,
            3 => Self::TenMinutes,
            4 => Self::OneHour,
            _ => Self::default(),
        }
    }
}
