/*  Copyright (C) 2023 Patrick Csikos (https://zelikos.dev)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authored by Patrick Csikos <pcsikos@zelikos.dev>
 */

use log::{debug, info};

use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::{gio, glib};

use crate::config::{APP_ID, PKGDATADIR, PROFILE, VERSION};
use crate::window::TypestWindow;

mod imp {
    use super::*;
    use glib::WeakRef;
    use once_cell::sync::OnceCell;

    // Holds state and widgets
    #[derive(Debug, Default)]
    pub struct TypestApplication {
        pub window: OnceCell<WeakRef<TypestWindow>>,
    }

    // Basics for GObject
    #[glib::object_subclass]
    impl ObjectSubclass for TypestApplication {
        const NAME: &'static str = "TypestApplication";
        type Type = super::TypestApplication;
        type ParentType = adw::Application;
    }

    // Overrides GObject vfuncs
    impl ObjectImpl for TypestApplication {}

    // Overrides GApplication vfuncs
    impl ApplicationImpl for TypestApplication {
        fn activate(&self) {
            debug!("GtkApplication<TypestApplication>::activate");
            self.parent_activate();
            let app = self.obj();

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.present();
                return;
            }

            let window = TypestWindow::new(&app);
            self.window
                .set(window.downgrade())
                .expect("Window already set.");

            app.main_window().present();
        }

        fn startup(&self) {
            debug!("GtkApplication<TypestApplication>::startup");
            self.parent_startup();
            let app = self.obj();

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_gactions();
            app.setup_accels();
        }
    }

    impl GtkApplicationImpl for TypestApplication {}
    impl AdwApplicationImpl for TypestApplication {}
}

glib::wrapper! {
    pub struct TypestApplication(ObjectSubclass<imp::TypestApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

#[allow(clippy::new_without_default)]
impl TypestApplication {
    fn main_window(&self) -> TypestWindow {
        self.imp().window.get().unwrap().upgrade().unwrap()
    }

    fn setup_gactions(&self) {
        // Quit
        let action_quit = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| {
                // This is needed to trigger the delete event and saving the window state
                app.main_window().close();
                app.quit();
            })
            .build();

        // About
        let action_about = gio::ActionEntry::builder("about")
            .activate(|app: &Self, _, _| {
                app.show_about();
            })
            .build();
        self.add_action_entries([action_quit, action_about]);
    }

    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Primary>Q"]);
    }

    fn show_about(&self) {
        let developers = ["Patrick Csikos <pcsikos@zelikos.dev>"];

        let designers = ["Brage Fuglseth"];

        // let artists = [""];

        // Translators: Replace "translator-credits" with your names, one per line
        let translators = &gettext("translator-credits");

        let copyright = "Copyright © 2023 Patrick Csikos";

        let about = adw::AboutWindow::from_appdata(
            &format!("/dev/zelikos/typest/{}.metainfo.xml", APP_ID),
            if PROFILE != "Devel" {
                Some(VERSION)
            } else {
                None
            },
        );

        if let Some(window) = self.active_window() {
            about.set_transient_for(Some(&window));
        }

        about.set_copyright(copyright);
        about.set_developers(&developers);
        about.set_designers(&designers);
        // about.set_artists(&artists);

        about.set_translator_credits(translators);

        // So we still get the specific commit in devel builds.
        if PROFILE == "Devel" {
            about.set_version(VERSION);
        }

        about.present();
    }

    pub fn run(&self) -> glib::ExitCode {
        info!("Raceway ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self)
    }
}

impl Default for TypestApplication {
    fn default() -> Self {
        glib::Object::builder()
            .property("application-id", APP_ID)
            .property("resource-base-path", "/dev/zelikos/typest/")
            .build()
    }
}
