<!-- ![Flathub](https://img.shields.io/flathub/v/dev.zelikos.rollit?style=for-the-badge) -->
<!-- ![Installs](https://img.shields.io/flathub/downloads/dev.zelikos.rollit?style=for-the-badge) -->

<!-- ![Icon](data/icons/hicolor/scalable/apps/dev.zelikos.rollit.svg) -->

> WARNING:
> This app is currently in a **very** early development and has many missing features.

# Raceway

Test your typing skills and practice to get better. Type random words from your chosen language, or input a custom text. See your WPM score (Words per Minute) and accuracy.

<!-- ![Screenshot](data/screenshots/01_rollit_startup_light.png) -->

## Installation

Raceway is currently in early development and does not yet have a stable release. The recommended installation method at this time is by following the build instructions below, but this is intended only for contributors, not end users.

<!-- The latest stable release of Roll-It is available via Flathub. -->

<!-- <a href='https://flathub.org/apps/details/dev.zelikos.rollit'><img width='240' alt='Download on Flathub' src='https://dl.flathub.org/assets/badges/flathub-badge-en.png'/></a> -->

Any version distributed elsewhere is not provided nor supported by me.

## Building

Raceway is built using GTK4 and libadwaita with the [GNOME HIG](https://developer.gnome.org/hig/) in mind.

If you would like to build and hack on Raceway, it is highly recommended to use [GNOME Builder](https://flathub.org/apps/org.gnome.Builder).

## Credits

Other GTK Rust projects used for reference:

- [GTK Rust Template](https://gitlab.gnome.org/World/Rust/gtk-rust-template)
- [Lorem](https://gitlab.gnome.org/World/design/lorem)

